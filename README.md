# FridgeStorage
FridgeStorage is a very simple android application written in
Java that helped my family to organize the contents of the
two fridges we had at the time. It provides the possibility to
save, delete and edit items as well as a search function.

![img: Main Activity](/assets/FS_landing_page.jpg)

---

![img: Main Activity](/assets/FS_database.jpg)

---

![img: Main Activity](/assets/FS_search.jpg)

---
