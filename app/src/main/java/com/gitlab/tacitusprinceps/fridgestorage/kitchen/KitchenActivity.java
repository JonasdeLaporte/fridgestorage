package com.gitlab.tacitusprinceps.fridgestorage.kitchen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.dbactivities.DrawersActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;

public class KitchenActivity extends AppCompatActivity {

    public final static String LOCATION = "Küche";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen);
    }

    public void kitchenTopClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_TOP);
        startActivity(intent);
    }

    public void kitchenMiddleClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_MIDDLE);
        startActivity(intent);
    }

    public void kitchenBottomClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_BOTTOM);
        startActivity(intent);
    }
}