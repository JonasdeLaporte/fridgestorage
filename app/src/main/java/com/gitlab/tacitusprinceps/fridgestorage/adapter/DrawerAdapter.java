package com.gitlab.tacitusprinceps.fridgestorage.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;

import java.util.List;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ItemViewHolder> {
    private final LayoutInflater mInflater;
    private List<Item> mItems; // Cached copy of words

    public DrawerAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_drawer, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if (mItems != null) {
            Item current = mItems.get(position);
            holder.mItemCountView.setText(Integer.toString(current.getCount()) + "x");
            holder.mItemNameView.setText(current.getName());
            holder.mItemAmountView.setText(current.getAmount());
            holder.mItemNoteView.setText(current.getNote());
        } else {
            // Covers the case of data not being ready yet.
            holder.mItemNameView.setText("No Item");
            holder.mItemAmountView.setText("");
        }
    }

    public void setWords(List<Item> items){
        mItems = items;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mItems != null)
            return mItems.size();
        else return 0;
    }

    public Item getItemAtPosition(int pos) {
        return mItems.get(pos);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView mItemCountView, mItemNameView, mItemAmountView, mItemNoteView;

        private ItemViewHolder(View itemView) {
            super(itemView);
            this.mItemCountView = itemView.findViewById(R.id.txtview_drawer_count);
            this.mItemNameView = itemView.findViewById(R.id.txtview_drawer_name);
            this.mItemAmountView = itemView.findViewById(R.id.txtview_drawer_amount);
            this.mItemNoteView = itemView.findViewById(R.id.txtview_drawer_note);
        }
    }
}
