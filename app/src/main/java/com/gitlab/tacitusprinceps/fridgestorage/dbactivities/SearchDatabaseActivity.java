package com.gitlab.tacitusprinceps.fridgestorage.dbactivities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.adapter.SearchAdapter;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;
import com.gitlab.tacitusprinceps.fridgestorage.data.ItemViewModel;

import java.util.List;

public class SearchDatabaseActivity extends AppCompatActivity {

    private ItemViewModel mItemViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_database);

        Intent intent = getIntent();
        String filter = "";
        filter = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        RecyclerView recyclerView = findViewById(R.id.recview_search);
        final SearchAdapter adapter = new SearchAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mItemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        mItemViewModel.getFilteredItems(filter).observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> items) {
                adapter.setWords(items);
            }
        });

        // Add the functionality to swipe items in the
        // recycler view to delete that item
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Item myItem = adapter.getItemAtPosition(position);

                        //TODO confirmation popup
                        Toast.makeText(SearchDatabaseActivity.this,
                                "'"+myItem.getName()+"' gelöscht!", Toast.LENGTH_LONG).show();

                        // Delete the word
                        mItemViewModel.delete(myItem);
                    }
                });

        helper.attachToRecyclerView(recyclerView);


    }
}