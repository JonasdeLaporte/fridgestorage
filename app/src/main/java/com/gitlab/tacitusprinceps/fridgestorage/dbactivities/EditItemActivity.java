package com.gitlab.tacitusprinceps.fridgestorage.dbactivities;

import androidx.appcompat.app.AppCompatActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.adapter.SearchAdapter;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class EditItemActivity extends AppCompatActivity {

    private EditText countEditText, nameEditText, amountEditText, noteEditText;
    private Spinner unitSpinner;
    private String countString, name, amount, unit, note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        Intent intent = getIntent();

        this.countEditText = findViewById(R.id.edittxt_edititem_count);
        this.nameEditText = findViewById(R.id.edittxt_edititem_name);
        this.amountEditText = findViewById(R.id.edittxt_edititem_amount);
        this.unitSpinner = findViewById(R.id.spinner_edititem_unit);
        this.noteEditText = findViewById(R.id.edittxt_edititem_note);

        int spinnerPos = 0;
        String unit = intent.getStringExtra(SearchAdapter.EXTRA_EDIT_UNIT);
        if (unit.equals("kg")) spinnerPos = 1;

        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this, R.array.units, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(mAdapter);

        this.countString = intent.getStringExtra(SearchAdapter.EXTRA_EDIT_COUNT);
        this.name = intent.getStringExtra(SearchAdapter.EXTRA_EDIT_NAME);
        this.amount = intent.getStringExtra(SearchAdapter.EXTRA_EDIT_AMOUNT);
        this.note = intent.getStringExtra(SearchAdapter.EXTRA_EDIT_NOTE);

        countEditText.setHint(countString);
        nameEditText.setHint(name);
        amountEditText.setHint(amount);
        unitSpinner.setSelection(spinnerPos);
        noteEditText.setHint(note);
    }

    public void btnEditItemAdd(View view) {
        // compare user input with existing values and update the values
        this.countString = this.getRelevantString(this.countString, this.countEditText.getText().toString());
        this.name = this.getRelevantString(this.name, this.nameEditText.getText().toString());
        this.amount = this.getRelevantString(this.amount, this.amountEditText.getText().toString());
        this.unit = this.unitSpinner.getSelectedItem().toString();
        this.note = this.getRelevantString(this.note, this.noteEditText.getText().toString());

        int count = Integer.parseInt(this.countString);

        Item updatedItem = new Item(count, this.name, this.amount + " " + this.unit,
                "", "", this.note);


    }

    private String getRelevantString(String orgValue, String newValue) {
        if (orgValue.equals(newValue)) return orgValue;
        if (newValue == null) return orgValue;
        if (newValue.isEmpty()) return orgValue;
        return newValue;
    }
}