package com.gitlab.tacitusprinceps.fridgestorage.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.gitlab.tacitusprinceps.fridgestorage.cellar.CellarActivity;
import com.gitlab.tacitusprinceps.fridgestorage.kitchen.KitchenActivity;

import java.util.List;

/**
 * This class acts as DataAccessObject (DAO) an contains all the queries to interact with the
 * item_table table
 *
 */
@Dao
public interface ItemDao {

    @Insert
    public void insert(Item item);

    @Delete
    public void deleteItem(Item item);

    @Query("DELETE FROM item_table")
    public void deleteAll();

    @Query("SELECT * FROM item_table ORDER BY name ASC")
    public LiveData<List<Item>> getAllItems();

    @Query("SELECT * FROM item_table WHERE name LIKE :filter ORDER BY name ASC")
    public LiveData<List<Item>> getFilteredItems(String filter);

    @Query("SELECT * FROM item_table WHERE location = :location AND drawer = :drawer ORDER BY name ASC")
    public LiveData<List<Item>> getSpecificItem(String location, String drawer);

    @Query("SELECT * FROM item_table WHERE location = '" + KitchenActivity.LOCATION + "' ORDER BY name ASC")
    public LiveData<List<Item>> getAllKitchenItems();

    @Query("SELECT * FROM item_table WHERE location = '" + CellarActivity.LOCATION + "' ORDER BY name ASC")
    public LiveData<List<Item>> getAllCellarItems();
}
