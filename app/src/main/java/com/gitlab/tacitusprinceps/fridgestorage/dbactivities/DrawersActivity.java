package com.gitlab.tacitusprinceps.fridgestorage.dbactivities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.adapter.DrawerAdapter;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;
import com.gitlab.tacitusprinceps.fridgestorage.data.ItemViewModel;

import java.util.List;

public class DrawersActivity extends AppCompatActivity {


    private ItemViewModel mItemViewModel;
    private String location;
    private String drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawers);

        Intent intent = getIntent();
        location = intent.getStringExtra(MainActivity.EXTRA_LOCATION);
        drawer = intent.getStringExtra(MainActivity.EXTRA_DRAWER);

        TextView header = findViewById(R.id.txtview_drawer_header);
        header.setText(location + " - " + drawer);

        RecyclerView recyclerView = findViewById(R.id.recview_drawers);
        final DrawerAdapter adapter = new DrawerAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mItemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        mItemViewModel.getItemsOfDrawerLocation(location, drawer).observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> items) {
                adapter.setWords(items);
            }
        });

        // Add the functionality to swipe items in the
        // recycler view to delete that item
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Item myItem = adapter.getItemAtPosition(position);

                        //TODO confirmation popup

                        // Delete the word
                        mItemViewModel.delete(myItem);
                    }
                });

        helper.attachToRecyclerView(recyclerView);
    }

    public void btnDrawerAddItem(View view) {
        Intent intent = new Intent(this, AddItemActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, location);
        intent.putExtra(MainActivity.EXTRA_DRAWER, drawer);
        startActivityForResult(intent, RESULT_OK);
    }
}