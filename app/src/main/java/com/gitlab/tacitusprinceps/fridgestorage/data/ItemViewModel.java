package com.gitlab.tacitusprinceps.fridgestorage.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gitlab.tacitusprinceps.fridgestorage.cellar.CellarActivity;
import com.gitlab.tacitusprinceps.fridgestorage.kitchen.KitchenActivity;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {

    private ItemRepository mRepository;
    private LiveData<List<Item>> mAllItems, mAllKitchenItems, mTopKitchenItems, mMiddleKitchenItems,
            mBottomKitchenItems, mAllCellarItems, mTopCellarItems, mMiddleCellarItems, mBottomCellarItems;

    public ItemViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ItemRepository(application);
        mAllItems = mRepository.getAllItems();
        mAllKitchenItems = mRepository.getAllKitchenItems();
        mAllCellarItems = mRepository.getAllCellarItems();
    }

    public LiveData<List<Item>> getAllItems() {
        return mAllItems;
    }

    public LiveData<List<Item>> getAllKitchenItems() {
        return mAllKitchenItems;
    }

    public LiveData<List<Item>> getAllCellarItems() {
        return mAllCellarItems;
    }

    //Special

    public LiveData<List<Item>> getItemsOfLocation(String location) {
        switch (location) {
            case KitchenActivity.LOCATION:
                return getAllKitchenItems();
            case CellarActivity.LOCATION:
                return getAllCellarItems();
            default:
                return getAllItems();
        }
    }

    public LiveData<List<Item>> getItemsOfDrawerLocation(String location, String drawer) {
        if (location != null && drawer != null) {
            return mRepository.getSpecificItem(location, drawer);
        } else {
            return getAllItems();
        }
    }

    public LiveData<List<Item>> getFilteredItems(String filter) {
        return mRepository.getFilteredItems(filter);
    }

    public void insert(Item item) {
        mRepository.insert(item);
    }

    public void delete(Item item) {mRepository.delete(item);}

}
