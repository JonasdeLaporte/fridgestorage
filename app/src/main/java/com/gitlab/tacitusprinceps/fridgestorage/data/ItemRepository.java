package com.gitlab.tacitusprinceps.fridgestorage.data;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ItemRepository {

    private ItemDao itemDao;
    private LiveData<List<Item>> mAllItems, mAllKitchenItems, mAllCellarItems;

    public ItemRepository(Application application) {
        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();

        mAllItems = itemDao.getAllItems();

        mAllKitchenItems = itemDao.getAllKitchenItems();
        mAllCellarItems = itemDao.getAllCellarItems();
    }

    public void insert(Item item) {
        new insertAsyncTask(itemDao).execute(item);
    }

    public void delete(Item item)  {
        new deleteAsyncTask(itemDao).execute(item);
    }


    public LiveData<List<Item>> getAllItems() {
        return mAllItems;
    }

    public LiveData<List<Item>> getFilteredItems(String filter) {
        if (filter.equals("")) return getAllItems();
        return itemDao.getFilteredItems("%" + filter + "%");
    }

    public LiveData<List<Item>> getSpecificItem(String location, String drawer) {
        return itemDao.getSpecificItem(location, drawer);
    }

    public LiveData<List<Item>> getAllKitchenItems() { return mAllKitchenItems; }

    public LiveData<List<Item>> getAllCellarItems() { return mAllCellarItems; }

    private static class insertAsyncTask extends AsyncTask<Item, Void, Void> {

        private ItemDao mAsyncTaskDao;

        public insertAsyncTask(ItemDao dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Item... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private  static class deleteAsyncTask extends AsyncTask<Item, Void, Void> {
        private ItemDao mAsyncTaskDao;

        deleteAsyncTask(ItemDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Item... params) {
            mAsyncTaskDao.deleteItem(params[0]);
            return null;
        }

    }
}
