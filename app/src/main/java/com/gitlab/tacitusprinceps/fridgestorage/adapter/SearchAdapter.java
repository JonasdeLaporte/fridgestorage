package com.gitlab.tacitusprinceps.fridgestorage.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;
import com.gitlab.tacitusprinceps.fridgestorage.dbactivities.EditItemActivity;
import com.gitlab.tacitusprinceps.fridgestorage.dbactivities.SearchDatabaseActivity;

import java.util.List;

import static com.gitlab.tacitusprinceps.fridgestorage.R.string.item_search_delete;
import static com.gitlab.tacitusprinceps.fridgestorage.R.string.item_search_edit;
import static com.gitlab.tacitusprinceps.fridgestorage.R.string.item_search_move;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ItemViewHolder>{
    public static final String EXTRA_EDIT_COUNT = "com.gitlab.tacitusprinceps.fridgestorage.adapter.COUNT";
    public static final String EXTRA_EDIT_NAME = "com.gitlab.tacitusprinceps.fridgestorage.adapter.NAME";
    public static final String EXTRA_EDIT_AMOUNT = "com.gitlab.tacitusprinceps.fridgestorage.adapter.AMOUNT";
    public static final String EXTRA_EDIT_UNIT = "com.gitlab.tacitusprinceps.fridgestorage.adapter.UNIT";
    public static final String EXTRA_EDIT_NOTE = "com.gitlab.tacitusprinceps.fridgestorage.adapter.NOTE";


    private final LayoutInflater mInflater;
    private List<Item> mItems; // Cached copy of words
    private Context context;

    public SearchAdapter(Context context) {
        this.context = context;
        //edit = context.getString(R.string.item_search_edit);
        mInflater = LayoutInflater.from(context); }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_search, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if (mItems != null) {
            Item current = mItems.get(position);
            holder.mItemCountView.setText(Integer.toString(current.getCount()) + "x");
            holder.mItemNameView.setText(current.getName());
            holder.mItemAmountView.setText(current.getAmount());
            holder.mItemLocationView.setText(current.getLocation());
            holder.mItemDrawerView.setText(current.getDrawer());
            //TODO note
            holder.mItemNoteView.setText(current.getNote());

        } else {
            // Covers the case of data not being ready yet.
            holder.mItemNameView.setText("No Item");
            holder.mItemAmountView.setText("");
        }
    }

    public void setWords(List<Item> items){
        mItems = items;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mItems != null)
            return mItems.size();
        else return 0;
    }

    public Item getItemAtPosition(int pos) {
        return mItems.get(pos);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView mItemCountView, mItemNameView, mItemAmountView, mItemLocationView,
                mItemDrawerView, mItemNoteView;

        private ItemViewHolder(View itemView) {
            super(itemView);
            this.mItemCountView = itemView.findViewById(R.id.txtview_search_count);
            this.mItemNameView = itemView.findViewById(R.id.txtview_search_name);
            this.mItemAmountView = itemView.findViewById(R.id.txtview_search_amount);
            this.mItemLocationView = itemView.findViewById(R.id.txtview_search_loc);
            this.mItemDrawerView = itemView.findViewById(R.id.txtview_search_drawer);
            this.mItemNoteView = itemView.findViewById(R.id.txtview_search_note);
            //itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Toast.makeText(context, "Long clicked item", Toast.LENGTH_LONG).show();
                    PopupMenu popupMenu = new PopupMenu(context, v);
                    popupMenu.getMenuInflater().inflate(R.menu.popupmenu_search, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            String title = item.getTitle().toString();

                            Toast.makeText(context, "You chose item " + title, Toast.LENGTH_SHORT).show();
                            String edit = context.getResources().getString(item_search_edit);
                            String move = context.getResources().getString(item_search_move);
                            String delete = context.getResources().getString(item_search_delete);

                            Intent intent;

                            if (title.equals(edit)) {
                                System.out.println("=== control: chose edit: " + edit);
                                intent = new Intent(context, EditItemActivity.class);

                                String amountAndUnit = mItemAmountView.getText().toString();
                                String[] splitAmount = amountAndUnit.split(" ");

                                String count = mItemCountView.getText().toString().split("x")[0];
                                String name = mItemNameView.getText().toString();
                                String amount = splitAmount[0];
                                String unit = splitAmount[1];
                                String note = mItemNoteView.getText().toString();

                                /*
                                System.out.println("COUNT of chosen item: >" + count.split("x")[0] + "<");
                                System.out.println("NAME of chosen item: " + name);
                                System.out.println("AMOUNT of chosen item: >" + amount + "<");
                                System.out.println("UNIT of chosen item: >" + unit + "<");
                                System.out.println("NOTE of chosen item: " + note);
                                */

                                intent.putExtra(EXTRA_EDIT_COUNT, count);
                                intent.putExtra(EXTRA_EDIT_NAME, name);
                                intent.putExtra(EXTRA_EDIT_AMOUNT, amount);
                                intent.putExtra(EXTRA_EDIT_UNIT, unit);
                                intent.putExtra(EXTRA_EDIT_NOTE, note);

                                context.startActivity(intent);

                            } else if (title.equals(move)) {
                                System.out.println("=== control: chose move: " + move);
                            } else if (title.equals(delete)) {
                                System.out.println("=== control: chose delete: " + delete);
                            } else {
                                Log.e("com.gitlab.tacitusprinceps.Adapter.SearchAdapter",
                                        "PopupMenu chose Item that should not exist!");
                            }
                            return true;
                        }
                    });
                    popupMenu.show();
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "Clicked item", Toast.LENGTH_LONG).show();
        }


    }
}
