package com.gitlab.tacitusprinceps.fridgestorage.dbactivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.data.Item;
import com.gitlab.tacitusprinceps.fridgestorage.data.ItemViewModel;

public class AddItemActivity extends AppCompatActivity {

    private ItemViewModel mItemViewModel;
    private String location, drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Intent intent = getIntent();
        this.location = intent.getStringExtra(MainActivity.EXTRA_LOCATION);
        this.drawer = intent.getStringExtra(MainActivity.EXTRA_DRAWER);

        mItemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);

        Spinner spinner = findViewById(R.id.spinner_additem_unit);
        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this, R.array.units, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(mAdapter);
        spinner.setSelection(0);
        System.out.println("SPINNER SELECTED ITEM POS: " + spinner.getSelectedItemPosition());
    }

    public void btnAddItemAdd(View view) {
        EditText editText_count = findViewById(R.id.edittxt_additem_count);
        EditText editText_name = findViewById(R.id.edittxt_additem_name);
        EditText editText_amount = findViewById(R.id.edittxt_additem_amount);
        EditText editText_note = findViewById(R.id.edittxt_additem_note);
        Spinner spinner = findViewById(R.id.spinner_additem_unit);
        //TODO count
        Editable count_editable = editText_count.getText();
        int count;
        if (count_editable.toString().equals("")) {
            count = 1;
        } else {
            count = Integer.parseInt(count_editable.toString());
        }
        String name = editText_name.getText().toString();
        String amount = editText_amount.getText().toString();
        String note = editText_note.getText().toString();
        Object selected_unit = spinner.getSelectedItem();
        if(selected_unit == null) {
            //TODO
        }
        String unit = selected_unit.toString();
        if (!amount.equals("")) amount = amount + " " + unit;
        Item item = new Item(count, name, amount, this.location, this.drawer, note);
        mItemViewModel.insert(item);

        setResult(RESULT_OK);

        finish();
    }
}