package com.gitlab.tacitusprinceps.fridgestorage.data;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.cellar.CellarActivity;
import com.gitlab.tacitusprinceps.fridgestorage.kitchen.KitchenActivity;


@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class ItemRoomDatabase extends RoomDatabase {
    public abstract ItemDao itemDao();
    private static ItemRoomDatabase INSTANCE;

    //*
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            //TODO change to onCreate to prevent wiping database on app open
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };
    //*/

    public static ItemRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ItemRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ItemRoomDatabase.class,
                            "item_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    //*
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private ItemDao mItemDao;

        private Item[] words = {
                new Item(5,"Pommes", "400 g", CellarActivity.LOCATION, MainActivity.DRAWER_TOP, ""),
                new Item(2,"Pizza", "200 g", KitchenActivity.LOCATION, MainActivity.DRAWER_MIDDLE, ""),
                new Item(10,"Fisch", "150 g", KitchenActivity.LOCATION, MainActivity.DRAWER_BOTTOM, ""),
                new Item(3,"Nudeln", "1 kg", CellarActivity.LOCATION, MainActivity.DRAWER_TOP, ""),
                new Item(1,"Schnitzel", "250 g", CellarActivity.LOCATION, MainActivity.DRAWER_MIDDLE,""),
                new Item(7,"Gemüse", "600 g", KitchenActivity.LOCATION, MainActivity.DRAWER_BOTTOM, ""),
                new Item(4,"Eiswürfel", "100 g", CellarActivity.LOCATION, MainActivity.DRAWER_TOP, ""),
                new Item(5,"Hefe", "4 kg", KitchenActivity.LOCATION, MainActivity.DRAWER_MIDDLE, ""),
                new Item(1,"Sauce", "300 g", CellarActivity.LOCATION, MainActivity.DRAWER_BOTTOM, "")
        };

        public PopulateDbAsync(ItemRoomDatabase db) {
            this.mItemDao = db.itemDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            //TODO remove dat
            mItemDao.deleteAll();

            for (int i = 0; i < words.length; i++) {
                Item item = words[i];
                mItemDao.insert(item);
            }
            return null;
        }

    }
    //*/
}
