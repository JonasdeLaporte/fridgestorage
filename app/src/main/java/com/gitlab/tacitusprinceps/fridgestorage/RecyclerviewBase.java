package com.gitlab.tacitusprinceps.fridgestorage;

import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.tacitusprinceps.fridgestorage.data.ItemViewModel;

public class RecyclerviewBase {

    private RecyclerView.Adapter mAdapter;
    private ItemViewModel mItemViewModel;

    public RecyclerviewBase(RecyclerView.Adapter adapter, ItemViewModel itemViewModel) {
        this.mAdapter = adapter;
        this.mItemViewModel = itemViewModel;
    }

    /*
    public void addDeleteSwipe() {
        // Add the functionality to swipe items in the
        // recycler view to delete that item
        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Item myItem = mAdapter.getItemAtPosition(position);

                        //TODO confirmation popup

                        // Delete the word
                        mItemViewModel.delete(myItem);
                    }
                });

        helper.attachToRecyclerView(recyclerView);
    }
    */
}
