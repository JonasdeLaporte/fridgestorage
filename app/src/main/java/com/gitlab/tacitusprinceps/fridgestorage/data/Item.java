package com.gitlab.tacitusprinceps.fridgestorage.data;

import android.media.audiofx.AcousticEchoCanceler;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * This class represents a table in the database, containing the stored food with following
 * columns:
 * <p>
 * +------+---------+--------+----------+------------+----------+---------+
 * |  ID  |  COUNT  |  NAME  |  AMOUNT  |  LOCATION  |  DRAWER  |  NOTE  |
 * +------+---------+--------+----------+------------+----------+---------+
 * <p>
 * ID: the auto generated primary key
 * COUNT: the physical amount of stored items of this kind
 * NAME: the name
 * AMOUNT: the quantity of one item in metric units like 'g', 'kg' etc.
 * LOCATION: the location of the item (kitchen/cellar)#
 * DRAWER: the drawer (top/middle/bottom)
 * <p>
 * possible columns:
 * - Unit: kg, g
 * - Edit
 * - Notes
 * - Delete
 */

@Entity(tableName = "item_table")
public class Item {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "count")
    private int mCount;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "amount")
    private String mAmount;

    @ColumnInfo(name = "location")
    private String mLocation;

    @ColumnInfo(name = "drawer")
    private String mDrawer;

    @ColumnInfo(name = "note")
    private String mNote;

    public Item(int count, String name, String amount, String location, String drawer, String note) {
        this.mCount = count;
        this.mName = name;
        this.mAmount = amount;
        this.mLocation = location;
        this.mDrawer = drawer;
        this.mNote = note;
    }

    public int getCount() {
        return mCount;
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getAmount() {
        return this.mAmount;
    }

    public String getLocation() {
        return this.mLocation;
    }

    public String getDrawer() {
        return mDrawer;
    }

    public String getNote() {
        return mNote;
    }

    public void setCount(int mCount) {
        this.mCount = mCount;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public void setLocation(String mLocation) {
        this.mLocation = mLocation;
    }

    public void setDrawer(String mDrawer) {
        this.mDrawer = mDrawer;
    }

    public void setNote(String mNote) {
        this.mNote = mNote;
    }
}
