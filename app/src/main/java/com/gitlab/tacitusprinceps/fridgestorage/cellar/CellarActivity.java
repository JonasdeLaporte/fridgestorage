package com.gitlab.tacitusprinceps.fridgestorage.cellar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.gitlab.tacitusprinceps.fridgestorage.MainActivity;
import com.gitlab.tacitusprinceps.fridgestorage.R;
import com.gitlab.tacitusprinceps.fridgestorage.dbactivities.DrawersActivity;

public class CellarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cellar);
    }

    public final static String LOCATION = "Keller";

    public void cellarTopClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_TOP);
        startActivity(intent);
    }

    public void cellarMiddleClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_MIDDLE);
        startActivity(intent);
    }

    public void cellarBottomClick(View view) {
        Intent intent = new Intent(this, DrawersActivity.class);
        intent.putExtra(MainActivity.EXTRA_LOCATION, LOCATION);
        intent.putExtra(MainActivity.EXTRA_DRAWER, MainActivity.DRAWER_BOTTOM);
        startActivity(intent);
    }
}