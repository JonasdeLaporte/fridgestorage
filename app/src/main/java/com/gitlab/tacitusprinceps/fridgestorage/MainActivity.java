package com.gitlab.tacitusprinceps.fridgestorage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.tacitusprinceps.fridgestorage.cellar.CellarActivity;
import com.gitlab.tacitusprinceps.fridgestorage.dbactivities.SearchDatabaseActivity;
import com.gitlab.tacitusprinceps.fridgestorage.kitchen.KitchenActivity;

/*
    TODO:
      - add edit functionality
      - test edge cases
      - additem activity in scroller
      - database list color
      -
 */

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.gitlab.tacitusprinceps.fridgestorage.MESSAGE";
    public static final String EXTRA_LOCATION = "com.gitlab.tacitusprinceps.fridgestorage.LOCATION";
    public static final String EXTRA_DRAWER = "com.gitlab.tacitusprinceps.fridgestorage.DRAWER";
    public static final String DRAWER_TOP = "Oben";
    public static final String DRAWER_MIDDLE = "Mitte";
    public static final String DRAWER_BOTTOM = "Unten";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void searchDatabase(View view) {
        Intent intent = new Intent(this, SearchDatabaseActivity.class);
        EditText editText = findViewById(R.id.edittxt_main_search);
        String msg = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, msg);
        startActivity(intent);
    }

    public void openKitchenActivity(View view) {
        Intent intent = new Intent(this, KitchenActivity.class);
        startActivity(intent);
    }

    public void openCellarActivity(View view) {
        Intent intent = new Intent(this, CellarActivity.class);
        startActivity(intent);
    }

    //https://developer.android.com/training/basics/firstapp/starting-activity#java
}